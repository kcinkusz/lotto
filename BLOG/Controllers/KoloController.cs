﻿using System.Web.Mvc;
using Repository.IRepo;
using System.Net;
using Microsoft.AspNet.Identity;
using System;
using Repository.Models;

namespace BLOG.Controllers
{
public class KoloController : Controller
{
    /// <summary>
    /// obiekt repozytorium -połączenie się do np bazy danych
    /// </summary>
    private readonly IPostsRepo _repo;
    /// <summary>
    /// Do kontrolera w definicjach wstrzykiwania wstrzykujemy odpowiednią instancje
    /// Możemy tutaj wstrzyknąć np obiekt fasady, który łączy ze sobą funkcjonalność pobierania
    /// danych z bazy oraz przetwarzania logiki
    /// </summary>
    /// <param name="repo"></param>
    public KoloController(IKoloPost repo)
    {
        _repo = repo;
    }
    /// <summary>
    /// Dla nauczyciela, po wywołaniu Indexu pobierane są kolokwia
    /// Dla ucznia, po wywołaniu Indexu wywołany jest
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public ActionResult Index(int? id)
    {
        if (id.HasValue)
        {
            //dla nauczyciela
            var kolo = _repo.GetAnswers(id.Value);
            //dla ucznia
            var kolo = _repo.GetKolo(id.Value);

            return View(kolo);
        }
        return;
    }

    //GET: Posts/Details/5
    /// <summary>
    /// Akcja do zwrócenia pozostałego czasu
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public ActionResult GetRemaningTime(int? id)
    {
        if (id == null)
        {
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }
        //tutaj wypełniamy cały model, ponieważ zawiera w sobie pole do pozostałego czasu
        Kolo kolo = _repo.GetRemaningTime((int)id);
        if (kolo == null)
        {
            return HttpNotFound();
        }
        return View(kolo);
    }
        
    /// <summary>
    /// Kontroler do utworzenia kolokwium
    /// </summary>
    /// <param name="kolo"></param>
    /// <returns></returns>
    [Authorize] //Kolo moze utworzyc tylko zalogowany uzytkonik
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Create([Bind(Include = "Title,Content")] Kolo kolo) //Bind oznacza ze tylko tytul i Kontent bedzie przesylany z przegladarki do serwera bo userId i data chcemy zeby sie automatycznie wygenerowala
    {
        if (ModelState.IsValid) //sprawdzamy czy dane wpisywane w formularz pasuja do tych ktore sa w mode bazy danych (typy i warotsci)
        {
            try
            {
                _repo.AddPost(kolo); //dodanie do bazy danych
                _repo.SaveChanges(); //zapisanie kontekstu (faktyczny zapis do bazy)
                return RedirectToAction("Index");
            }
            catch
            {
                return View(kolo);
            }
        }

        return View(kolo);
    }
}
}
