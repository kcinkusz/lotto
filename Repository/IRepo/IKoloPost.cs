﻿using Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Blog.kololotto.BuissnessClasses;

namespace Repository.IRepo
{
public interface IKoloPost
{
    /// <summary>
    /// Pobierz odpowiedzi z kolokwium, metoda dedykowana
    /// nauczycielowi
    /// </summary>
    /// <id>numer/kod kolokwium</id>
    /// <returns></returns>
    IQueryable<Kolo> GetAnswers(int id);
    /// <summary>
    /// Pobierz pozostały czas, metoda do pobierania z serwera
    /// pozostałego czasu kolokwium
    /// </summary>
    /// <param name="id">numer/kod kolokwium</param>
    /// <returns></returns>
    string GetRemaningTime(int id);
    /// <summary>
    /// Czy kolokwium jest jeszcze aktywne
    /// </summary>
    /// <param name="id">numer/kod kolokwium</param>
    /// <returns></returns>
    bool IsEnabled(int id);
    /// <summary>
    /// Pobierz czas startu
    /// </summary>
    /// <returns></returns>
    DateTime GetStartTime(int id);
    /// <summary>
    /// Pobierz czas zakończenia
    /// </summary>
    /// <param name="id">numer/kod kolokwium</param>
    /// <returns></returns>
    DateTime GetEndTime(int id);
    /// <summary>
    /// Pobierz kolokwium
    /// </summary>
    /// <param name="id">numer/kod kolokwium</param>
    /// <returns></returns>
    Kolo GetKolo(int id);
}
}
